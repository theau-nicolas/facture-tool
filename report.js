//barchart | ████▓▓▓▓▓▒▒░░

import axios from "axios"
import { formatMonth, formatShortMonth, maxTotal, percent, totalHoursByMonth, totalInvoiceByMonth, totalInvoiceReport } from "./utils.js"
import { HBarGraph, VBarGraph, displayReportTitle } from "./display.js"

// console.log("▓")


const getTotalByMonth = async () => {
    const invoices = await axios.get("http://localhost:1337/api/factures")
    const tab = (totalInvoiceByMonth(invoices.data.data))
    const max = maxTotal(tab)

    console.log("FACTURED BY MONTH")
    console.log("")
    console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
    console.log("year  month    | FACTURED")

    console.log(" ")

    HBarGraph({
        tab,
        maxValue: max,
        attributeName: "total",
        labels: ['year', 'month'],
        displayedValue: "€"
    })

    console.log("________________________________")

    console.log(" ")
    console.log(" ")
    console.log(" ")
    console.log(" ")

    VBarGraph({
        tab,
        maxValue: max,
        attributeName: "total",
        labels: ['year', 'month'],
        displayedValue: "€",
        formatLabelFunction: formatShortMonth,
        spacing: 2
    })


}

const getHoursByMonth = async () => {
    const invoices = await axios.get("http://localhost:1337/api/factures")
    const tab = (totalHoursByMonth(invoices.data.data))
    const max = maxTotal(tab)

    console.log("HOURS BY MONTH")
    console.log("")
    console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯")
    console.log("year  hours    | HOURS")

    console.log(" ")
    console.log(" ")

    tab.forEach(element => {
        var bar = ""
        for (let i = 0; i < (Math.trunc(percent(element.total, max)) / 10); i++) {
            bar += "▓"
        }
        bar += "▓▓▒"
        console.log(element.year + " " + formatMonth(element.month) + " | " + bar + "    " + element.total + "€")
    });

    console.log("________________________________")

    console.log(" ")
    console.log(" ")
    console.log(" ")
    console.log(" ")
}


const main = async () => {
    displayReportTitle()
    getTotalByMonth()
    // getHoursByMonth()



}

main()