export const months = [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre",
];

export function monthToString(mois) { return months[mois]; }

export function genMonths(year) {
    const months = [];

    for (let i = 0; i < 12; i++) {
        const month = i + 1;
        const startDate = new Date(year, i, 1).toISOString().substring(0, 10);
        const endDate = new Date(year, i + 1, 0).toISOString().substring(0, 10);
        months.push({ start: startDate, end: endDate });
    }

    return months;
}

export function transposeMonth(month) {
    const index = months.findIndex((m) => m.toLowerCase() === month.toLowerCase());
    return index !== -1 ? index + 1 : null;
}

export function getStartEndDate(year, month) {
    const premierJour = new Date(year, month - 1, 1);
    const dernierJour = new Date(year, month, 0);

    return {
        start: premierJour.toISOString().substring(0, 10),
        end: dernierJour.toISOString().substring(0, 10)
    };
}

export function extractLabels(tab) {
    const labels = tab.map(object => `${object.id}/${object.attributes.label}`);
    return labels;
}


export function groupByLabel(tab) {
    const groupes = {};

    for (const object of tab) {
        const label = object.attributes.label;
        const quantity = object.attributes.quantity;

        if (groupes[label]) {
            groupes[label].quantity += quantity;
        } else {
            groupes[label] = {
                label: label,
                quantity: quantity
            };
        }
    }

    return Object.values(groupes);
}

export function calculatePrice(tab, target, contractUnitPrice) {
    const result = []
    var total = 0
    tab.forEach(line => {
        total += (contractUnitPrice * line.quantity)
        result.push({ ...line, target, unitPrice: contractUnitPrice, total: contractUnitPrice * line.quantity })
    });
    return { lines: result, total }
}


// REPORTING ++++++++++++++ ///

export function totalInvoiceReport(tab) {
    var total = 0
    tab.forEach(invoice => {
        total += invoice.attributes.total_ht
    });
    return total
}


export function totalInvoiceByMonth(tab) {
    let totalPerMonth = {};
    tab.forEach(object => {
        const mois = new Date(object.attributes.from).getUTCMonth();
        const year = new Date(object.attributes.from).getFullYear();
        const stringMonth = monthToString(mois);
        const totalHT = object.attributes.total_ht;
        const key = `${stringMonth}-${year}`;
        if (!totalPerMonth.hasOwnProperty(key)) {
            totalPerMonth[key] = {
                month: stringMonth,
                year: year,
                total: totalHT
            };
        } else {
            totalPerMonth[key].total += totalHT;
        }
    });

    return Object.values(totalPerMonth);
}

export function totalHoursByMonth(tab) {
    let totalPerMonth = {};
    tab.forEach(object => {
        const mois = new Date(object.attributes.from).getUTCMonth();
        const year = new Date(object.attributes.from).getFullYear();
        const stringMonth = monthToString(mois);
        const totalHT = object.attributes.quantity;
        const key = `${stringMonth}-${year}`;
        if (!totalPerMonth.hasOwnProperty(key)) {
            totalPerMonth[key] = {
                month: stringMonth,
                year: year,
                total: totalHT
            };
        } else {
            totalPerMonth[key].total += totalHT;
        }
    });

    return Object.values(totalPerMonth);
}

export function maxTotal(tableau) {
    let max = 0;
    for (let i = 0; i < tableau.length; i++) {
        if (tableau[i].total > max) {
            max = tableau[i].total;
        }
    }
    return max;
}

export function percent(chiffre, max) {
    return (chiffre / max) * 100;
}


export function formatMonth(month) {
    const maxLength = Math.max(...months.map(m => m.length));
    const paddedMonth = month.padEnd(maxLength, " ");
    return paddedMonth;
}

export function formatShortMonth(month, year) {
    return month.substring(0, 3) + "| " + year.toString().substring(2, 4)
}

export const formatBarLabel = (label) => {
    return label.padEnd(Math.max(...months.map(m => m.length)) + 5, " ");
}