var pdf = require("pdf-creator-node");
var fs = require("fs");
const axios = require('axios').default;

// Read HTML Template
var html = fs.readFileSync("template.html", "utf8");

var document = {
    html: html,
    path: "./factures/output.pdf",
    type: "",
};


var options = {
    format: "A3",
    orientation: "portrait",
    border: "10mm",
    header: {
        height: "45mm",
    },
    footer: {
        height: "28mm",
        contents: {
            default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value

        }
    }
};



function genMonths(year) {
    const months = [];

    for (let i = 0; i < 12; i++) {
        const month = i + 1;
        const startDate = new Date(year, i, 1).toISOString().substring(0, 10);
        const endDate = new Date(year, i + 1, 0).toISOString().substring(0, 10);
        months.push({ start: startDate, end: endDate });
    }

    return months;
}

const getContracts = async () => {
    const result = await axios.get("http://localhost:1337/api/contracts?populate=*&filters[provider][id][$eq]=1")
    // console.log(result.data.data)
    // console.log(result.data.meta)
    return result.data.data
}

const getPrestations = async (month, contract, id) => {
    const { start, end } = month
    // const result = await axios.get(`http://localhost:1337/api/prestations?populate=*&filters[$and][0][execution_date][$gte]=${start}T04:00:00.000Z&filters[$and][1][execution_date][$lte]=${end}T04:00:00.000Z&filters[$and][2][contract][id]=${contract.id}`)
    const provider = await axios.get(`http://localhost:1337/api/providers/${contract.attributes.provider.data.id}?populate=*`)
    const client = await axios.get(`http://localhost:1337/api/clients/${contract.attributes.client.data.id}?populate=*`)

    // console.log(client.data.data)

    // document.path = `./factures/output.pdf`

    document.data = {
        date: month.end,
        provider: provider.data.data.attributes,
        client: client.data.data.attributes
    }

    // const fact = await pdf.create(document, options)

    // console.log(fact)
    pdf
        .create(document, options)
        .then((res) => {
            console.log(res);
        })
        .catch((error) => {
            console.error(error);
        });

}

// getPrestations()

// contracts.forEach(contract => {

//     getPrestations(contract, month)
// });

const main = async () => {
    const months = genMonths(2022)
    const contracts = await getContracts()

    console.log(months)

    // months.forEach(month => {
    //     contracts.forEach(contract => {
    //         getPrestations(month, contract)
    //     })
    // })

    // console.log(document.data)
    // const fact = await pdf.create(document, options)
    // console.log(fact)

}




main()
