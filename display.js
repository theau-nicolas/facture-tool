import { DateTime } from "luxon"
import { formatBarLabel, formatMonth, formatShortMonth, percent } from "./utils.js"
import chalk from 'chalk';

export function displayReportTitle() {
    console.log(" ")
    console.log(" ")


    console.log(` .d888                  888                                  888 `)
    console.log(`d88P"                   888                                  888 `)
    console.log(`888                     888                                  888 `)
    console.log(`888888 8888b.   .d8888b 888888 888  888 888d888 .d88b.   .d88888 `)
    console.log(`888       "88b d88P"    888    888  888 888P"  d8P  Y8b d88" 888 `)
    console.log(`888   .d888888 888      888    888  888 888    88888888 888  888 `)
    console.log(`888   888  888 Y88b.    Y88b.  Y88b 888 888    Y8b.     Y88b 888`)
    console.log(`888   "Y888888  "Y8888P  "Y888  "Y88888 888     "Y8888   "Y88888`)
    console.log(" ")

    console.log("                                 _   ")
    console.log("                                ( )_ ")
    console.log(" _ __   __   _ _      _    _ __ | ,_)")
    console.log("( '__)/'__`\\( '_ \\  /'_`\\ ( '__)| |  ")
    console.log("| |  (  ___/| (_) )( (_) )| |   | |_ ")
    console.log("(_)  `\\____)| ,__/'`\\___/'(_)   `\\__)")
    console.log("            | |                      ")
    console.log("            (_)")

    console.log(" ")
    console.log("DATE : " + DateTime.now().toFormat('MMMM dd, yyyy'))
    console.log("Made by Théau NICOLAS")
    console.log(" ")
    console.log(" ")

}


export const HBarGraph = ({ tab, maxValue, attributeName, labels, displayedValue }) => {
    tab.forEach(element => {
        var bar = ""
        for (let i = 0; i < Math.trunc((percent(element[attributeName], maxValue)) / 10); i++) {
            bar += "▓"
        }
        bar += "▓▓▒"

        var label = ""
        labels.forEach(la => {
            label += element[la] + " "
        });

        label = formatBarLabel(label)

        console.log(chalk.greenBright(label + " | " + bar + "      " + element[attributeName] + displayedValue))
    });

}

export const VBarGraph = ({ tab, maxValue, attributeName, labels, displayedValue, formatLabelFunction, spacing = 5 }) => {
    var lines = []
    var space = ""
    for (let s = 0; s < spacing; s++) {
        space += " "
    }

    for (let i = 12; i > 0; i--) {
        var line = ""
        tab.forEach(element => {
            const value = Math.trunc(percent(element[attributeName], maxValue) / 10)
            if (value === i - 2) {
                line += element[attributeName] + displayedValue
            }
            if (value >= i) {
                line += " ▓▓▓▓▒ " + space
            }
        })
        lines.push(line)
    }

    lines.forEach(element => {
        console.log(chalk.greenBright(element))
    })

    var label = ""
    var divider = ""
    tab.forEach(element => {
        divider += "_______" + space
        label += formatLabelFunction(element[labels[1]], element[labels[0]]) + space
    });

    console.log(divider)
    console.log("")
    console.log(label)
    console.log("")





    // var line = []
    // tab.forEach(element => {
    //     var bar = ""
    //     for (let i = 0; i < (Math.trunc(percent(element[attributeName], maxValue)) / 10); i++) {
    //         bar += ("▓▓▒   ")
    //     }
    //     line.push(bar)
    // })

    // line.forEach(l => {
    //     console.log(l)

    // });
}