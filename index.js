import inquirer from 'inquirer';
import axios from "axios"
import fs from "node:fs"
import { months, transposeMonth, getStartEndDate, extractLabels, groupByLabel, calculatePrice } from './utils.js';
import pdf from "pdf-creator-node"
import { hasher } from "node-object-hash"
import { DateTime } from 'luxon';

import signale from 'signale'

var params = {
    vat: 0
}
var html = fs.readFileSync("template.html", "utf8");
var options = {
    format: "A3",
    orientation: "portrait",
    border: "20mm",
    header: {
        height: "5mm",
    },
    footer: {
        height: "28mm",
        contents: {
            default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value

        }
    }
};




const getContracts = async () => {
    const result = await axios.get("http://localhost:1337/api/contracts?populate=*&filters[provider][id][$eq]=1")
    return result.data.data
}

const main = async () => {
    const contracts = await getContracts()
    const prompt = await inquirer
        .prompt([
            {
                type: 'list',
                name: 'year',
                message: 'Which year?',
                choices: ["2021", "2022", "2023"],
            },
            {
                type: "list",
                name: 'month',
                message: "Which month?",
                choices: months
            },
            {
                type: "list",
                name: 'contracts',
                message: "Which contract?",
                choices: extractLabels(contracts)
            },
            {
                name: 'invoice_number',
                message: 'What number?'
            },
        ])
    const dates = getStartEndDate(prompt.year, transposeMonth(prompt.month))

    const contract = contracts.filter((el) => el.id == prompt.contracts.split('/')[0])[0]
    const provider = await axios.get(`http://localhost:1337/api/providers/${contract.attributes.provider.data.id}?populate=*`)
    const client = await axios.get(`http://localhost:1337/api/clients/${contract.attributes.client.data.id}?populate=*`)
    const prestations = await axios.get(`http://localhost:1337/api/prestations?populate=*&filters[$and][0][execution_date][$gte]=${dates.start}T04:00:00.000Z&filters[$and][1][execution_date][$lte]=${dates.end}T04:00:00.000Z&filters[$and][2][contract][id]=${contract.id}`)

    const lines = calculatePrice(groupByLabel(prestations.data.data), contract.attributes.target, contract.attributes.unit_price)

    params = { ...params, dates, contract, provider: provider.data.data.attributes, client: client.data.data.attributes, prestations: lines.lines, total: lines.total }

    // hashin to signature //
    var hashSort = hasher({ sort: true, coerce: false });
    var signature = hashSort.hash(params)
    // ------------------- //

    // now have to verify if this invoice already exist, if not, find a invoice number //
    const fromInvoice = await axios.get(`http://localhost:1337/api/factures?filters[signature][$eq]=${signature}`)

    // if there nothing then the invoice hasn't been created earlier, so i can create it
    if (!(fromInvoice.data.data.length > 0)) {

        const create = await axios.post("http://localhost:1337/api/factures", {
            data: {
                number: prompt.invoice_number,
                emisson_date: DateTime.now().toFormat('MMMM dd, yyyy'),
                total_ht: params.total,
                total_ttc: params.total - (params.vat * params.total),
                vat: params.vat,
                signature,
                from: params.dates.start,
                to: params.dates.end
            }
        })

        params = { ...params, invoice_number: prompt.invoice_number }

    } else {
        signale.warn(`Invoice has been already created with number : ${fromInvoice.data.data[0].attributes.number}.`)
        params = { ...params, invoice_number: fromInvoice.data.data[0].attributes.number }
        signale.pending(`changing invoice number "${prompt.invoice_number}" to "${fromInvoice.data.data[0].attributes.number}"`)
    }

    var document = {
        html: html,
        path: `./factures/Facture - ${params.invoice_number}.pdf`,
        type: "",
        data: params
    };



    const invoice = await pdf.create(document, options)
    console.log({ ...invoice, hash: signature })
}

main()

